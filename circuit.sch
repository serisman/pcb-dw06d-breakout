EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:IC
LIBS:circuit-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "13 aug 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_2 K1
U 1 1 53CB049B
P 4400 3950
F 0 "K1" V 4350 3950 50  0000 C CNN
F 1 "CONN_2" V 4450 3950 40  0000 C CNN
F 2 "" H 4400 3950 60  0000 C CNN
F 3 "" H 4400 3950 60  0000 C CNN
	1    4400 3950
	-1   0    0    1   
$EndComp
$Comp
L CONN_2 K2
U 1 1 53CB04AA
P 7350 3950
F 0 "K2" V 7300 3950 50  0000 C CNN
F 1 "CONN_2" V 7400 3950 40  0000 C CNN
F 2 "" H 7350 3950 60  0000 C CNN
F 3 "" H 7350 3950 60  0000 C CNN
	1    7350 3950
	1    0    0    -1  
$EndComp
$Comp
L IC-6 U1
U 1 1 53CB06FA
P 5950 3950
F 0 "U1" H 5950 4250 60  0000 C CNN
F 1 "DW06D" H 5950 3650 60  0000 C CNN
F 2 "~" H 5950 3950 60  0000 C CNN
F 3 "~" H 5950 3950 60  0000 C CNN
	1    5950 3950
	1    0    0    -1  
$EndComp
Text Label 6400 3950 0    60   ~ 0
VDD
Text Label 4750 4050 0    60   ~ 0
P+
Text Label 4750 3850 0    60   ~ 0
P-
$Comp
L R R1
U 1 1 53EB846B
P 6700 4200
F 0 "R1" V 6780 4200 40  0000 C CNN
F 1 "100" V 6707 4201 40  0000 C CNN
F 2 "~" V 6630 4200 30  0000 C CNN
F 3 "~" H 6700 4200 30  0000 C CNN
	1    6700 4200
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 53EB847A
P 5250 3950
F 0 "R2" V 5330 3950 40  0000 C CNN
F 1 "1k" V 5257 3951 40  0000 C CNN
F 2 "~" V 5180 3950 30  0000 C CNN
F 3 "~" H 5250 3950 30  0000 C CNN
	1    5250 3950
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 53EB84A5
P 6700 3750
F 0 "C1" H 6700 3850 40  0000 L CNN
F 1 "100n" H 6706 3665 40  0000 L CNN
F 2 "~" H 6738 3600 30  0000 C CNN
F 3 "~" H 6700 3750 60  0000 C CNN
	1    6700 3750
	1    0    0    -1  
$EndComp
Text Label 7000 3850 2    60   ~ 0
B-
Text Label 5500 3850 2    60   ~ 0
V-
Text Label 5250 4200 0    60   ~ 0
P-
Text Label 7000 4050 2    60   ~ 0
P+
Connection ~ 6700 3550
Wire Wire Line
	6400 3550 6400 3850
Connection ~ 6700 3950
Connection ~ 7000 3550
Wire Wire Line
	7000 4050 7000 4450
Connection ~ 7000 4450
Wire Wire Line
	7000 3550 7000 3850
Wire Wire Line
	7000 4450 6700 4450
Wire Wire Line
	5500 3850 5500 3700
Wire Wire Line
	5500 3700 5250 3700
Wire Wire Line
	5500 4050 5500 4350
Connection ~ 6400 3550
Wire Wire Line
	5500 3950 5400 3950
Wire Wire Line
	5400 3950 5400 4200
Wire Wire Line
	5400 4200 5250 4200
Wire Wire Line
	5100 4350 5100 3550
Connection ~ 5100 3550
Connection ~ 5100 4350
Wire Wire Line
	5500 4350 5100 4350
NoConn ~ 6400 4050
Wire Wire Line
	5100 3550 7000 3550
Wire Wire Line
	6700 3950 6400 3950
$EndSCHEMATC
