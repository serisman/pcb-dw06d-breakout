# DW06D - Lithium Cell Protection Breakout v1.0 #

## Schematic ##

![Schematic.png](https://bytebucket.org/serisman/pcb-dw06d-breakout/raw/master/output/Schematic.png)

## Design Rules ##

* Clearance: 7 mil
* Track Width: 20 mil
* Via Diameter: 27 mil
* Via Drill: 13 mil
* Zone Clearance: 7 mil
* Zone Min Width: 7 mil
* Zone Thermal Antipad Clearance: 7 mil
* Zone Thermal Spoke Width: 15 mil

## PCB available on OSH Park ##

* 0.4" x 0.25" (10.21 mm x 6.40 mm)
* $0.1667 each ($0.50 for 3)
* [https://oshpark.com/shared_projects/JM1YQUxn](https://oshpark.com/shared_projects/JM1YQUxn)

### PCB Front ###

![PCB - Front.png](https://bytebucket.org/serisman/pcb-dw06d-breakout/raw/master/output/PCB%20-%20Front.png)

### PCB Back ###

![PCB - Back.png](https://bytebucket.org/serisman/pcb-dw06d-breakout/raw/master/output/PCB%20-%20Back.png)